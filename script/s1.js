table = $('#tablebody')
$.ajax({
  url: `https://api.spacexdata.com/v3/launches/past?sort=launch_date_utc&order=desc&limit=3`,
}).done(data => {
  for(i in data){
    el = data[i]
    table.append(`<tr><td>${el.flight_number}</td><td>${el.mission_name}</td><td>${el.launch_site.site_name_long}</td><td>${new Date(1000*el.launch_date_unix)}</td><td><img style="width: 300px" src="${el.links.flickr_images[0]}" alt=""></td><td><a href="${el.links.article_link}"><i class="fas fa-link"></i></a></td></tr>`)
  }
})
